﻿using System;
namespace Animales {
    public class Program
    {
        static void Main(string[] args)
        {
            Perro perrito = new Perro();
            Gato gatito = new Gato();

            string ruidoperro;
            string ruidogato;
            Console.WriteLine("Insertar el ruido de un perro");
            ruidoperro = Console.ReadLine();
            bool p = perrito.Comparar(ruidoperro);


            Console.WriteLine("Insertar el ruido de un gato");
            ruidogato = Console.ReadLine();
            bool g = perrito.Comparar(ruidoperro);

        }
    }
    public abstract class Animal
    {
        public abstract string HaceRuido();

    } 
    public class Perro : Animal
    { 
        private string ruido = "Guau";
        public Perro()
        {

        }
        public override string HaceRuido()
        {
            return ruido;
        }
        public bool Comparar(string ruidoperro)
        {
            return this.ruido == ruidoperro;
        }
    }
    public class Gato : Animal
        {
        private string ruido = "Miau";
        public Gato()
        {
        }
        public override string HaceRuido()
        {
            return ruido;
        }
        public bool Comparar(string ruidogato)
        {
            return this.ruido == ruidogato;
        }
    }
}