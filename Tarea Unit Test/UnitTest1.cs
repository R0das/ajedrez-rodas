using Animales;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace Tarea_Unit_Test
{
    [TestClass]
    public class Tests
    {

        [Test]
        public void Test1()
        {
            Perro perrito2 = new Perro();
            bool result = perrito2.Comparar("Miau");
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("Guau", result);
        }

        [Test]
        public void Test2()
        {
            Gato gatito2 = new Gato();
            bool result = gatito2.Comparar("Guau");
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("Miau", result);
        }
    }
}